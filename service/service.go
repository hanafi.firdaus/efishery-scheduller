package service

import (
	"context"
	"efishery-scheduller/helper"
	"efishery-scheduller/model"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/jinzhu/gorm"
	"github.com/labstack/gommon/log"
	"github.com/robfig/cron"
)

var ctx = context.Background()

// Handler is
type Handler struct {
	Redis *redis.Client
	DB    *gorm.DB
}

// Service is
func Service(r *redis.Client, gorm *gorm.DB) {
	Handler := &Handler{
		Redis: r,
		DB:    gorm,
	}

	c := cron.New()

	Handler.GetDataKursDollar()

	// Setiap hari tiap 8 jam sekali
	c.AddFunc("* */8 * * *", func() {
		Handler.GetDataKursDollar()
	})

	c.Start()
}

// GetDataKursDollar is
func (h Handler) GetDataKursDollar() {
	url := helper.Getenv("API_CONVERT_TO_USD")

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf(err.Error())
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf(err.Error())
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf(err.Error())
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(string(body)), &result)

	kursValue := result["USD_IDR"].(map[string]interface{})["val"].(float64)
	err = h.Redis.Set(ctx, "kursDollar", kursValue, time.Hour*24).Err()
	if err != nil {
		log.Printf(err.Error())
	}

	var us model.KursUSD

	us.Price = kursValue
	err = h.DB.Create(&us).Error
	if err != nil {
		log.Printf(err.Error())
	}
}
