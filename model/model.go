package model

import "github.com/jinzhu/gorm"

type (

	// KursUSD is
	KursUSD struct {
		gorm.Model
		Price float64 `gorm:"type:numeric" json:"uuid"`
	}
)

// TableName is
func (KursUSD) TableName() string {
	return "kurs_usd"
}
