package config

import (
	"context"
	"efishery-scheduller/helper"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

// Redis is
func (config *Config) Redis() (err error) {
	client := redis.NewClient(&redis.Options{
		Addr:     helper.Getenv("URL_REDIS"),
		Password: "",
		DB:       0,
	})

	config.redis = client

	_, err = client.Ping(ctx).Result()
	if err != nil {
		return
	}
	return
}
