package config

import (
	"efishery-scheduller/helper"
	"efishery-scheduller/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Postgresql is
func (cfg *Config) Postgresql() (err error) {

	connectionString := helper.Getenv("GORM_CONNECTION")
	db, err := gorm.Open("postgres", connectionString)
	if err != nil || db.Error != nil {
		return
	}

	db.CreateTable(&model.KursUSD{})

	cfg.gorm = db
	return
}
