# Scheduller efishery

## Fungsi
```
Untuk update kurs dolar per hari sebanyak 3 kali update ke redis, serta menyimpan history data kurs dolar ke database
```

## Spesifikasi
1. Postgresql
2. Redis
3. Gorm (ORM database golang)

## Petunjuk penggunaan
1. Install redis
2. export GO111MODULE=on (Untuk mengaktifkan go module)
3. Install PostgreSQL 12.3 (Ubuntu 12.3-1.pgdg18.04+1)
4. Gunakan golang versi go1.13.8
5. Untuk menjalankan project go menggunakan command ```go run .```
6. Jika ada error ketik cronjob menjalankan fungsi ```GetDataKursDollar``` coba ganti param value apikey ```API_CONVERT_TO_USD``` yang berada di file .env dikarenakan kena limit untuk get kurs dolarnya.

## Setting database
1. Buat db dengan nama db efishery
2. Untuk username dan password bisa menyesuaikan atau mengikuti setting database di file .env