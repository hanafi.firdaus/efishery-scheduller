package helper

import "os"

// Getenv is
func Getenv(key string) (fallback string) {
	value := os.Getenv(key)
	if len(value) == 0 {
		if key == "GORM_CONNECTION" {
			fallback = "host=localhost port=5432 user=postgres dbname=efishery sslmode=disable password=hhjsdjERT768&%5^$#^&&^DRhsjgDHfwhg@#7V56353HfDmHfhFFFcBahdhT"
		} else if key == "GORM_LOG" {
			fallback = "true"
		} else if key == "API_CONVERT_TO_USD" {
			fallback = "https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=y&apiKey=fb7393d2fa538b9ee7ca"
		} else if key == "URL_REDIS" {
			fallback = "localhost:6379"
		}
		return fallback
	}
	return value
}
